import http.server

PORT = int(os.environ.get('PORT', 5500))

class MyHandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header("Access-Control-Allow-Origin", "https://replit.com")
        self.send_header("Access-Control-Allow-Methods", "GET, PING, OPTIONS")
        self.send_header("Content-Type", "text/html")
        self.end_headers()
        self.wfile.write(b"<h3>Copy me, the url above!</h3>")

def start_server():
    server_address = ('', PORT)
    httpd = http.server.HTTPServer(server_address, MyHandler)
    print("Server for UptimeRobot is ready!")
    httpd.serve_forever()

if __name__ == '__main__':
    start_server()