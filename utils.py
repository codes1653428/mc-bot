import random
import time

def sleep(ms):
    time.sleep(ms / 1000)

def getRandom(arr):
    return random.choice(arr)