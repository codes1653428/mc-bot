import mineflayer
from utils import sleep, getRandom
import json

loop = None
bot = None

def disconnect():
    global loop, bot
    if loop:
        loop.cancel()
    if bot:
        bot.quit()
        bot.end()

async def reconnect():
    print(f"Trying to reconnect in {CONFIG['action']['retryDelay'] / 1000} seconds...\n")
    disconnect()
    await sleep(CONFIG['action']['retryDelay'])
    createBot()

def createBot():
    global bot
    bot = mineflayer.createBot({
        'host': CONFIG['client']['host'],
        'port': int(CONFIG['client']['port']),
        'username': CONFIG['client']['username']
    })
    bot.once('error', lambda error: print(f"AFKBot got an error: {error}"))
    bot.once('kicked', lambda rawResponse: print(f"\n\nAFKbot is disconnected: {rawResponse}"))
    bot.once('end', lambda: reconnect())
    bot.once('spawn', lambda: changePos())

async def changePos():
    lastAction = getRandom(CONFIG['action']['commands'])
    halfChance = True if random.random() < 0.5 else False
    print(f"{lastAction}{' with sprinting' if halfChance else ''}")
    bot.setControlState('sprint', halfChance)
    bot.setControlState(lastAction, True)
    await sleep(CONFIG['action']['holdDuration'])
    bot.clearControlStates()